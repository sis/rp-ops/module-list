# module-list

This repo hosts `module-list.md`. The file is automatically created on the customapps client 
(nightly cron job with the `operator` user). The job exports a up-to-date list of the current software version and
pushes it to this repo.
The list is then used in the user docs: https://unlimited.ethz.ch/display/LeoMed2/List+of+centrally+installed+software